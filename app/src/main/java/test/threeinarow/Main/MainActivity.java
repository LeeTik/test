package test.threeinarow.Main;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import java.io.File;
import java.util.Random;

import test.threeinarow.R;
import test.threeinarow.Screen.GameScreen;


public class MainActivity extends Activity {

    EditText editTextN,editTextM;
    Toast toast;
    private SharedPreferences MySave;
    private GameScreen gameScreen;

    int n, m;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextN = (EditText) findViewById(R.id.editTextN);
        editTextM = (EditText) findViewById(R.id.editTextM);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (gameScreen!=null)
        {
            MySave=getSharedPreferences("Save", Context.MODE_PRIVATE);
            gameScreen.SaveGame(MySave);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Release the Camera because we don't need it when paused
        // and other activities might need to use it.
        if (gameScreen!=null)
        {
            MySave=getSharedPreferences("Save", Context.MODE_PRIVATE);
            gameScreen.SaveGame(MySave);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        gameScreen.Destroy();
        gameScreen=null;
    }

    public void onClickLoad(View view)
    {
            MySave=getSharedPreferences("Save", Context.MODE_PRIVATE);
            Load();
    }

    public void onClickStartSquare(View view) {
        if (checkNandM()) {
            MySizeScreen();

            CreateColor();

            gameScreen = new GameScreen(this, n, m, 0);
            setContentView(gameScreen);
        }

    }

    public void onClickStartHexagon(View view) {
       if (checkNandM())
       {
            MySizeScreen();

            CreateColor();

            gameScreen=new GameScreen(this,n,m,1);
            setContentView(gameScreen);
        }
    }

    void MySizeScreen()
    {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Assest.heigthSreen=metrics.heightPixels;
        Assest.wigthScreen=metrics.widthPixels;
    }

    private boolean checkNandM()
    {
        n=0;
        m=0;
        if (editTextN.getText().toString().trim().length() > 0) n=Integer.parseInt(editTextN.getText().toString());
        if (editTextM.getText().toString().trim().length() > 0) m=Integer.parseInt(editTextM.getText().toString());
        if (n<3 || m<3)
        {
            toast=Toast.makeText(getApplicationContext(),"не введены n и m или n и m < 3!", Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }
        return true;
    }

    private void CreateColor()
    {
        Assest.myColor=new Assest.MyColor[m];
        Random randomcolor = new Random();
        for (int i=0; i<m; i++)
        {
            Assest.myColor[i]=new Assest.MyColor();
            Assest.myColor[i].x=randomcolor.nextInt(255);
            Assest.myColor[i].y=randomcolor.nextInt(255);
            Assest.myColor[i].z=randomcolor.nextInt(255);

        }
    }

    private void Load()
    {
        MySave=getSharedPreferences("Save", Context.MODE_PRIVATE);
        n=MySave.getInt("N",0);
        m=MySave.getInt("M",0);

        int mode=MySave.getInt("mode",0);

        MySizeScreen();

        Assest.myColor=new Assest.MyColor[m];
        for (int i=0; i<m; i++)
        {
            Assest.myColor[i]=new Assest.MyColor();
            Assest.myColor[i].x=MySave.getInt("color"+i+"x",0);
            Assest.myColor[i].y=MySave.getInt("color"+i+"y",0);
            Assest.myColor[i].z=MySave.getInt("color"+i+"z",0);

        }

        gameScreen=new GameScreen(this,n,m,mode,MySave);

        setContentView(gameScreen);
    }
}
