package test.threeinarow.Actor;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import test.threeinarow.Main.Assest;


/**
 * Created by Valera on 20.02.2015.
 */
public class Token {

    private enum Select{NoTouch, YesTouch}
    private Select SelectDraw;
    private float x,y; //координаты
    private float UpdateX,UpdateY;
    private float SpeedX,SpeedY;
    private float speed=15;  //скорость перемещения расстояние/speed
    private float radius;  //радиус
    private int color;     //номер цвета
    private int colorRGB; /// цвет в RGB
    private boolean RunUpdate=false;

    public Token(float x,float y, float radius, int color)
    {
        this.x=x;
        this.y=y;
        this.radius=radius;

        this.color=color;

        this.colorRGB=Color.rgb(Assest.myColor[color].x, Assest.myColor[color].y, Assest.myColor[color].z);
        SelectDraw=Select.NoTouch;
    }

    public void Draw (Canvas canvas, Paint paint)
    {
        update();
        if (SelectDraw==Select.YesTouch)
        {
            paint.setColor(Color.BLACK);
            canvas.drawCircle(x,y,radius+7,paint);
        }
        paint.setColor(colorRGB);
        canvas.drawCircle(x,y,radius,paint);

    }

    private void  update()
    {
        if (RunUpdate)
        {
            if (Math.abs(x-UpdateX)>1) x+=SpeedX;
            if (Math.abs(y-UpdateY)>1) y+=SpeedY;
            if (Math.abs(x-UpdateX)<1 && Math.abs(y-UpdateY)<1)
            {
                RunUpdate=false;
                this.x=UpdateX;
                this.y=UpdateY;
            }
        }
    }

    public Token hit(float x, float y)
    {
        return ((x-this.x)*(x-this.x)+(y-this.y)*(y-this.y)<radius*radius)? this : null;
    }

     public float getX ()
    {
        return x;
    }

    public float getY ()
    {
        return y;
    }

    public void setUpdateX(float x)
    {
        UpdateX=x;
        SpeedX=(x-this.x)/speed;
    }

    public void setUpdateY(float y)
    {
        UpdateY=y;
        SpeedY=(y-this.y)/speed;
    }

    public boolean getRunUpdate()
    {
        return RunUpdate;
    }

    public float getRadius()
    {
        return radius;
    }

    public void  setX(float x)
    {
        this.x=x;
    }

    public void setY (float y)
    {
        this.y=y;
    }

    public void setSelectDraw()
    {
        if (SelectDraw==Select.NoTouch)
            SelectDraw=Select.YesTouch;
        else SelectDraw=Select.NoTouch;
    }

    public int getColor()
    {
        return color;

    }

    public void setColor(int color)
    {
        this.color=color;
        colorRGB=Color.rgb(Assest.myColor[color].x, Assest.myColor[color].y, Assest.myColor[color].z);
    }

    public void StartAnimation(float x, float y)
    {
        setUpdateX(x);
        setUpdateY(y);
        RunUpdate=true;
    }
}
