package test.threeinarow.Screen;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import test.threeinarow.Main.Assest;
import test.threeinarow.Stage.Field;
import test.threeinarow.Stage.FieldHexagon;
import test.threeinarow.Stage.FieldSquare;
import test.threeinarow.Stage.Cell;
/**
 * Created by Valera on 20.02.2015.
 */
public class GameScreen extends SurfaceView implements SurfaceHolder.Callback{

    private DrawThread drawThread;
    private LogicThread logicThread;
    private Field field;
    private Paint paint;
    private int mode;
    private boolean newField=false;

    private Cell CellStatus =null, CellStatus2 =null;

    private Cell Animationtoken1=null, Animationtoken2=null;

    public GameScreen(Context context,int n,int m,int mode) {
        super(context);
        this.mode=mode;
        newScreen(n,m,mode);

    }
    public GameScreen(Context context,int n,int m,int mode,SharedPreferences MySave) {
        super(context);

        this.mode=mode;

        newScreen(n,m,mode);

        for (int i=0; i< field.getFieldSize(); i++)
        {
            for (int j=0; j< field.getFieldSize(); j++)
            {
                field.getCell()[i][j].getToken().setColor(MySave.getInt("token" + i + j, 0));
            }
        }

        field.setScore(MySave.getInt("Score", 0));

        for (int i=0; i<field.getFieldSize()+1; i++)
        {
            field.setScoreArray(i,MySave.getInt("Score"+i, 0));
        }

        mode=MySave.getInt("mode", 0);


    }

    private void newScreen(int n , int m,int mode)
    {
        getHolder().addCallback(this);

        paint = new Paint();

        if (mode==0)field = new FieldSquare(n,m);
        else field=new FieldHexagon(n,m);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        drawThread = new DrawThread(getHolder(),this);
        drawThread.setRunning(true);
        drawThread.start();
        logicThread= new LogicThread(this);
        logicThread.setRunning(true);
        logicThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        drawThread.setRunning(false);
        logicThread.setRunning(false);
        while (retry) {
            try {
                drawThread.join();
                logicThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if (action==MotionEvent.ACTION_DOWN && !field.CheckALlAnimation()){
            ///если мы выбрали элемент
            if (field.hit(event.getX(),event.getY())!=null) {
                //// запоминаем его
                if (CellStatus == null) {
                    CellStatus = field.hit(event.getX(), event.getY());
                    CellStatus.getToken().setSelectDraw();
                }
                else {
                    ////проверяем другая ли эта фишка
                    /// если да, делеаем ещё проверки рядом ли фишки и будет ли это полезным шагом
                    /// если да то меняем фишки местами
                    CellStatus2 = field.hit(event.getX(), event.getY());
                    if (CellStatus != CellStatus2 && field.BesideToken(CellStatus.getToken(), CellStatus2.getToken())
                            && field.CheckStep(CellStatus,CellStatus2)
                            )
                    {
                        float x,y;
                        CellStatus.getToken().setSelectDraw();
                        x= CellStatus.getToken().getX();
                        y= CellStatus.getToken().getY();
                        CellStatus.getToken().StartAnimation(CellStatus2.getToken().getX(), CellStatus2.getToken().getY());
                        CellStatus2.getToken().StartAnimation(x, y);
                        /////

                        Animationtoken1= CellStatus;
                        Animationtoken2= CellStatus2;

                        ///
                        CellStatus =null;
                        CellStatus2 =null;
                    }
                    else
                    {
                        Toast.makeText(getContext(),"Нет хода", Toast.LENGTH_SHORT).show();
                        if (CellStatus !=null) CellStatus.getToken().setSelectDraw();
                        CellStatus =null;

                    }
                }

            }
            else
            {
                if (CellStatus !=null) CellStatus.getToken().setSelectDraw();
                CellStatus =null;
            }

        }

        return true;
    }

    /////главный метод рисования, он используется в классе DrawThread
    public void Draw(Canvas canvas){

        paint.setColor(Color.RED);

        paint.setStrokeWidth(30);

        paint.setTextSize(40);

        field.Draw(canvas, paint);

    }

    /////главный метод логики игры, он используется в классе LogicThread
    public void Logic()
    {

        update();
        field.Logic();
    }

    private void update()
    {
        if (Animationtoken1!=null)
        {
            if (!Animationtoken1.getToken().getRunUpdate())
            {
            field.SwapCell(Animationtoken1.getToken(), Animationtoken2.getToken());
            Animationtoken1=null;
            Animationtoken2=null;
            }
        }
        //обновления поля если нет ходов
        if (!field.checkAllStep() && !field.CheckALlAnimation())
        {

            field.newField();
        }

    }

    ////сохранение состояния
    public void SaveGame(SharedPreferences MySave)
    {
        SharedPreferences.Editor editor = MySave.edit();
        editor.putInt("N", field.getFieldSize());
        editor.putInt("M", field.getMaxColor());
        /// загрузка цветов
        for (int i=0; i< field.getMaxColor(); i++)
        {
            editor.putInt("color"+i+"x", Assest.myColor[i].x);
            editor.putInt("color"+i+"y", Assest.myColor[i].y);
            editor.putInt("color"+i+"z", Assest.myColor[i].z);
        }
        ///загрузка цвета фишек
        for (int i=0; i< field.getFieldSize(); i++)
        {
            for (int j=0; j< field.getFieldSize(); j++)
            {
                editor.putInt("token" +i+j, field.getCell()[i][j].getToken().getColor() );
            }
        }
        ///счет
        for (int i=0; i<field.getFieldSize()+1; i++)
        {
            editor.putInt("Score"+i,field.getScoreArray()[i]);
        }
        editor.putInt("Score", field.getScore());
        editor.putInt("mode", mode);
        editor.apply();
    }

    public void Destroy()
    {
        field.Destroy();
        field=null;
    }

    public void StopGame()
    {
        drawThread.setRunning(false);
        logicThread.setRunning(false);
    }
}
