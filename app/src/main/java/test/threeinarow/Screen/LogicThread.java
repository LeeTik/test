package test.threeinarow.Screen;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;

/**
 * Created by Valera on 20.02.2015.
 */
    public class LogicThread extends Thread {
    private GameScreen gameScreen;
    private boolean running = false;

    public LogicThread(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    @Override
    public void run() {
        while (running) {
            gameScreen.Logic();
        }
    }
}

