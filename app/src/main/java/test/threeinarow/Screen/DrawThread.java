package test.threeinarow.Screen;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;

/**
 * Created by Valera on 20.02.2015.
 */
////// класс для отрисовки////////
public class DrawThread extends Thread
{
    private GameScreen gameScreen;
    private boolean running = false;
    private SurfaceHolder surfaceHolder;

    public DrawThread(SurfaceHolder surfaceHolder,GameScreen gameScreen ) {
        this.surfaceHolder = surfaceHolder;
        this.gameScreen=gameScreen;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    @Override
    public void run() {
        Canvas canvas=null;
        while (running) {
            canvas = null;
            try {
                canvas = surfaceHolder.lockCanvas(null);
                if (canvas == null)
                    continue;
                canvas.drawColor(Color.WHITE);
                gameScreen.Draw(canvas);
            } finally {
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }


}