package test.threeinarow.Stage;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

import test.threeinarow.Actor.Token;
import test.threeinarow.Main.Assest;

/**
 * Created by Valera on 20.02.2015.
 */
public class FieldSquare extends Field {

    Random randomcolor;

    public FieldSquare(int height,int color)
    {
        this.FieldSize=height;
        this.MaxColor=color;

        Score=0;

        ScoreArray=new int [FieldSize+1];
        for (int i=0; i<FieldSize+1;i++) ScoreArray[i]=0;
        CheckCell=new byte[FieldSize*FieldSize];
        randomcolor= new Random();
        cell=new Cell[FieldSize][FieldSize];

        CalculationMulti();

        CreateField();

     }

    ////public
    ///// главный метод рисования//////
    @Override
    public void Draw(Canvas canvas,Paint paint)
    {

        DrawLineField(canvas, paint);  //// рисование таблицы

        paint.setColor(Color.BLACK);

        paint.setTextSize(40);

        canvas.drawText(" Score= " + Score, Assest.wigthScreen/2,100,paint);

        for (int i =0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                if (cell[i][j]!=null) cell[i][j].Draw(canvas,paint);  /// рисование всех ячеек
            }
        }
    }

    ////private/////

    @Override
    protected Cell CreateCell (int i , int j)
    {
        float x,y;
        float radius;

        x=Xstart+multiWidth*j;
        y=Ystart+multiHeight*i;
        radius=(multiWidth-StrokeWidthLine*3)/2;

        return new Cell(x,y,radius,randomcolor.nextInt(MaxColor));
    }

    private void CalculationMulti()
    {
        multiWidth=(Assest.wigthScreen-2*indent)/(FieldSize+1);
        multiHeight=multiWidth;
        Xstart=indent+multiWidth;
        Ystart=(Assest.heigthSreen-Assest.wigthScreen-2*indent)/2+multiWidth;
    }

    //// проверка находяться ли фишки рядом
    @Override
    public boolean BesideToken (Token a, Token b)
    {
        return (
                (Math.abs(a.getX() - b.getX()) < (multiWidth + 5) && Math.abs(a.getY()-b.getY())<1) ||
                        (Math.abs(a.getY() - b.getY()) < (multiHeight + 5) && Math.abs(a.getX() - b.getX())<1)
        );
    }

    //// главный метод логики игры
    @Override
    public void Logic()
    {
        if (cell!=null && !CheckALlAnimation())
        {
            CheckToken(); /// проверка на удаление
            AddNewCell(); /// добавление недостающих
        }
    }

    ///проверка всех ходов
    @Override
    public boolean checkAllStep()
    {
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize-1; j++)
            {
                if (CheckStep(cell[i][j],cell[i][j+1])) return true;
                if (CheckStep(cell[j][i],cell[j+1][i])) return true;
            }
        }
        return false;
    }
    ///проверка возможности на ход
    @Override
    public boolean CheckStep(Cell a , Cell b)
    {
        Cell[][] c= newCells(a,b);  /// создаем нувую структуру cell с будушем ходом
        boolean check=false;
        ////проверяем будут ли эти ходы полезными(получиться ли линия более 2 фишек)
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                if (c[i][j].getToken().getX()==a.getToken().getX() &&c[i][j].getToken().getY()==a.getToken().getY() ||
                        c[i][j].getToken().getX()==b.getToken().getX() &&c[i][j].getToken().getY()==b.getToken().getY() )
                {
                    if (CheckOneTokenUpAndDown(c, c[i][j],i,j )>2) check=true;
                    if (CheckOneTokenLeftAndRight(c, c[i][j],i,j )>2) check=true;
                }
            }
        }
        c=null;
        return check;
    }
    //// private ////

    ///// рисования таблицы/////
    private void DrawLineField(Canvas canvas, Paint paint)
    {
        paint.setColor(Color.BLACK);

        paint.setStrokeWidth(StrokeWidthLine);

        for (int i=0; i<FieldSize+1; ++i)
        {
            canvas.drawLine(Xstart-(multiWidth/2)+multiWidth*i,Ystart-(multiHeight/2),Xstart-(multiWidth/2)+multiWidth*i,Ystart-(multiHeight/2)+multiHeight*(FieldSize),paint);
            canvas.drawLine(Xstart-(multiWidth/2),Ystart-(multiHeight/2)+multiHeight*i,Xstart-(multiWidth/2)+multiWidth*(FieldSize),Ystart-(multiHeight/2)+multiHeight*i,paint);
        }

    }


  ///проверка на удаление
    private void CheckToken()
    {
        int l;
        for (int i=0; i<FieldSize*FieldSize; i++) CheckCell[i]=0;
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                l=CheckOneTokenUpAndDown(cell, cell[i][j], i, j);
                if (l>2)
                {
                    DeleteTokensUpAndDown(cell,cell[i][j],i,j);
                    ScoreArray[l]++;
                }
                l=CheckOneTokenLeftAndRight(cell, cell[i][j], i, j);
                if (l>2)
                {
                    DeleteTokensLeftAndRight(cell, cell[i][j], i, j);
                    ScoreArray[l]++;
                }
            }
        }
        for (int i=0; i<FieldSize*FieldSize; i++)
        {
            if (CheckCell[i]==1) {
                cell[i / FieldSize][i - (i / FieldSize) * FieldSize].setToken(null);
                cell[i / FieldSize][i - (i / FieldSize) * FieldSize] = null;
                Score++;

            }
        }
    }

    ////check up and down///
    private int CheckOneTokenUpAndDown(Cell[][] cells, Cell a,int StartI, int StartJ)
    {
        int l=1;
        int i1=StartI;
        while (i1>0)
        {
            i1--;
            if (cells[i1][StartJ].getToken().getColor()==a.getToken().getColor())
            {
                l++;
            }
            else break;
        }
        i1=StartI;
        while (i1<(FieldSize-1))
        {
            i1++;
            if (cells[i1][StartJ].getToken().getColor()==a.getToken().getColor())
            {
                l++;
            }
            else break;
        }
        return l;
    }

    private void DeleteTokensUpAndDown(Cell[][] cells,Cell a, int StartI, int StartJ)
    {
        int i1=StartI;
        while (i1>0)
        {
            i1--;
            if (cells[i1][StartJ].getToken().getColor()==a.getToken().getColor())
            {
                CheckCell[i1*FieldSize+StartJ]=1;
            }
            else break;
        }
        i1=StartI;
        while (i1<(FieldSize-1))
        {
            i1++;
            if (cells[i1][StartJ].getToken().getColor()==a.getToken().getColor())
            {
                CheckCell[i1*FieldSize+StartJ]=1;
            }
            else break;
        }

        CheckCell[StartI*FieldSize+StartJ]=1;
    }

    //// check left and right
    private int CheckOneTokenLeftAndRight(Cell[][] cells, Cell a,int StartI, int StartJ)
    {
        int l=1;
        int j1=StartJ;
        while (j1>0)
        {
            j1--;
            if (cells[StartI][j1].getToken().getColor()==a.getToken().getColor())
            {
                l++;
            }
            else break;
        }
        j1=StartJ;
        while (j1<FieldSize-1)
        {
            j1++;
            if (cells[StartI][j1].getToken().getColor()==a.getToken().getColor())
            {
                l++;
            }
            else break;
        }
        return l;
    }

    private void DeleteTokensLeftAndRight(Cell[][] cells, Cell a,int StartI, int StartJ)
    {
        int j1=StartJ;
        while (j1>0)
        {
            j1--;
            if (cells[StartI][j1].getToken().getColor()==a.getToken().getColor())
            {
                CheckCell[StartI*FieldSize+j1]=1;
            }
            else break;
        }
        j1=StartJ;
        while (j1<FieldSize-1)
        {
            j1++;
            if (cells[StartI][j1].getToken().getColor()==a.getToken().getColor())
            {
                CheckCell[StartI*FieldSize+j1]=1;
            }
            else break;
        }

        CheckCell[StartI*FieldSize+StartJ]=1;
    }






}
