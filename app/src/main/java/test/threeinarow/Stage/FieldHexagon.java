package test.threeinarow.Stage;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

import test.threeinarow.Actor.Token;
import test.threeinarow.Main.Assest;

/**
 * Created by Valera on 21.02.2015.
 */
public class FieldHexagon extends Field {

    Random randomcolor;

    public FieldHexagon(int height,int color)
    {
        this.FieldSize=height;
        this.MaxColor=color;

        Score=0;

        ScoreArray=new int [FieldSize+1];
        for (int i=0; i<FieldSize+1;i++) ScoreArray[i]=0;
        CheckCell=new byte[FieldSize*FieldSize];
        randomcolor= new Random();
        cell=new Cell[FieldSize][FieldSize];

        CalculationMulti();

        CreateField();
    }

    @Override
    public void Draw(Canvas canvas,Paint paint)
    {

        DrawLineField(canvas, paint);  //// рисование таблицы

        paint.setColor(Color.BLACK);

        paint.setTextSize(40);

        canvas.drawText(" Score= " + Score, Assest.wigthScreen/2,100,paint);

        for (int i =0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                if (cell[i][j]!=null) cell[i][j].Draw(canvas,paint);  /// рисование всех ячеек
            }
        }

    }

    private void DrawLineField(Canvas canvas, Paint paint)
    {
        float radius=(multiWidth)/2;
        float k;
        paint.setColor(Color.BLACK);

        paint.setStrokeWidth(StrokeWidthLine);

        /*
        for (int i=0; i<FieldSize+1; ++i)
        {
            canvas.drawLine(Xstart-(multiWidth/2)+multiWidth*i,Ystart-(multiHeight/2),Xstart-(multiWidth/2)+multiWidth*i,Ystart-(multiHeight/2)+multiHeight*(FieldSize),paint);
            canvas.drawLine(Xstart-(multiWidth/2),Ystart-(multiHeight/2)+multiHeight*i,Xstart-(multiWidth/2)+multiWidth*(FieldSize),Ystart-(multiHeight/2)+multiHeight*i,paint);
        }

*/
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                k=j%2==1?+radius:0;
                DrawHexagon(canvas,paint,Xstart+(float)Math.sqrt(radius*radius*3)*j,Ystart+(multiWidth)*i+k,radius);
            }
        }

    }

    //строю шестиугольник, r - радиус вписанной окружности
    private void DrawHexagon(Canvas canvas, Paint paint,float X, float Y, float r)
    {
        float side=(float)(r*2.0/Math.sqrt(3));

        canvas.drawLine(X-side/2,Y+r,X+side/2,Y+r,paint);
        canvas.drawLine(X-side/2,Y+r,X-r,Y,paint);
        canvas.drawLine(X-r,Y,X-side/2,Y-r,paint);
        canvas.drawLine(X-side/2,Y-r,X+side/2,Y-r,paint);
        canvas.drawLine(X+side/2,Y-r,X+r,Y,paint);
        canvas.drawLine(X+r,Y,X+side/2,Y+r,paint);
    }

    @Override
    public boolean BesideToken (Token a, Token b)
    {
        return Math.sqrt((a.getX() - b.getX())*(a.getX() - b.getX())+(a.getY()-b.getY())*(a.getY()-b.getY())) <multiWidth*1.5;
    }

    @Override
    protected Cell CreateCell (int i , int j)
    {
        float x,y;
        float radius;
        float k;
        radius=(multiWidth-StrokeWidthLine*3)/2;

        x=Xstart+(float)Math.sqrt((multiWidth)*(multiWidth)/4*3)*j;
        y=Ystart+multiHeight*i;

        k=j%2==1?multiHeight/2:0;

        return new Cell(x,y+k,radius,randomcolor.nextInt(MaxColor));
    }

    //логика игры
    @Override
    public void Logic()
    {
        if (cell!=null && !CheckALlAnimation())
        {
            CheckToken(); /// проверка на удаление
            AddNewCell(); /// добавление недостающих
        }
    }

    @Override
    public boolean CheckStep(Cell a , Cell b)
    {
        Cell[][] c= newCells(a,b);  /// создаем нувую структуру cell с будушем ходом
        boolean check=false;
        ////проверяем будут ли эти ходы полезными(получиться ли линия более 2 фишек)
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                if (Math.abs(c[i][j].getToken().getX()-a.getToken().getX())<1 && Math.abs(c[i][j].getToken().getY()-a.getToken().getY())<1  ||
                        Math.abs(c[i][j].getToken().getX()-b.getToken().getX())<1 && Math.abs(c[i][j].getToken().getY()-b.getToken().getY())<1 )
                {
                    if (CheckOneTokenUpAndDown(c, c[i][j], i, j)>2) check=true;
                    if (CheckOneTokenLeftDownAndRightUp(c, c[i][j], i, j)>2) check=true;
                    if (CheckOneTokenLeftUpAndRightDown(c, c[i][j], i, j)>2) check=true;
                }
            }
        }
        c=null;
        return check;
    }

    ///проверка всех ходов
    @Override
    public boolean checkAllStep()
    {
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize-1; j++)
            {
                if (CheckStep(cell[j][i],cell[j+1][i])) return true;
            }
        }
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                if (j%2==0)
                {
                    if (i>0 && j<FieldSize-1)
                        if (CheckStep(cell[i][j],cell[i-1][j+1])) return true;

                    if (j<FieldSize-1)
                        if (CheckStep(cell[i][j],cell[i][j+1])) return true;

                    if (j>0)
                        if (CheckStep(cell[i][j],cell[i][j-1])) return true;

                    if (i>0 && j>0)
                        if (CheckStep(cell[i][j],cell[i-1][j-1])) return true;
                }
                else
                {
                    if (j<FieldSize-1)
                        if (CheckStep(cell[i][j],cell[i][j+1])) return true;

                    if (i<FieldSize-1 && j<FieldSize-1)
                        if (CheckStep(cell[i][j],cell[i+1][j+1])) return true;

                    if (i<FieldSize-1 && j>0)
                        if (CheckStep(cell[i][j],cell[i+1][j-1])) return true;

                    if (j>0)
                        if (CheckStep(cell[i][j],cell[i][j-1])) return true;
                }
            }
        }
        return false;
    }

    //вычисление
    private void CalculationMulti()
    {
        multiWidth=(Assest.wigthScreen-2*indent)/(FieldSize);
        multiHeight=multiWidth;
        Xstart=indent+multiWidth/1.5f;
        Ystart=(Assest.heigthSreen-Assest.wigthScreen-2*indent)/2;
    }

    ///проверка на удаление
    private void CheckToken()
    {
        int l;
        for (int i=0; i<FieldSize*FieldSize; i++) CheckCell[i]=0;
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                  l=CheckOneTokenUpAndDown(cell, cell[i][j], i, j);
                  if (l>2)
                  {
                      DeleteTokensUpAndDown(cell,cell[i][j],i,j);
                      ScoreArray[l]++;
                  }
                  l=CheckOneTokenLeftDownAndRightUp(cell, cell[i][j], i, j);
                  if (l>2)
                  {
                      DeleteTokensLeftDownAndRightUp(cell, cell[i][j], i, j);
                      ScoreArray[l]++;
                  }
                  l=CheckOneTokenLeftUpAndRightDown(cell, cell[i][j], i, j);
                  if (l>2)
                  {
                      DeleteTokensLeftUpAndRightDown(cell, cell[i][j], i, j);
                      ScoreArray[l]++;
                  }
            }
        }
        for (int i=0; i<FieldSize*FieldSize; i++)
        {
            if (CheckCell[i]==1) {
                cell[i / FieldSize][i - (i / FieldSize) * FieldSize].setToken(null);
                cell[i / FieldSize][i - (i / FieldSize) * FieldSize] = null;
                Score++;
            }
        }
    }
    ////check up and down///
    private int CheckOneTokenUpAndDown(Cell[][] cells, Cell a,int StartI, int StartJ)
    {
        int l=1;
        int i1=StartI;
        while (i1>0)
        {
            i1--;
            if (cells[i1][StartJ].getToken().getColor()==a.getToken().getColor())
            {
                l++;
            }
            else break;
        }
        i1=StartI;
        while (i1<(FieldSize-1))
        {
            i1++;
            if (cells[i1][StartJ].getToken().getColor()==a.getToken().getColor())
            {
                l++;
            }
            else break;
        }
        return l;
    }

    private void DeleteTokensUpAndDown(Cell[][] cells,Cell a, int StartI, int StartJ)
    {
        int i1=StartI;
        while (i1>0)
        {
            i1--;
            if (cells[i1][StartJ].getToken().getColor()==a.getToken().getColor())
            {
                CheckCell[i1*FieldSize+StartJ]=1;
            }
            else break;
        }
        i1=StartI;
        while (i1<(FieldSize-1))
        {
            i1++;
            if (cells[i1][StartJ].getToken().getColor()==a.getToken().getColor())
            {
                CheckCell[i1*FieldSize+StartJ]=1;
            }
            else break;
        }

        CheckCell[StartI*FieldSize+StartJ]=1;
    }

    //// check leftDown and rightUp
    private int CheckOneTokenLeftDownAndRightUp(Cell[][] cells, Cell a,int StartI, int StartJ)
    {
        int l=1;
        int j1=StartJ;
        int i1=StartI;
        while (j1<FieldSize-1 && i1>=0)
        {
            j1++;
            if  (j1%2==1) i1--;
            if (i1<0) break;
            if (cells[i1][j1].getToken().getColor()==a.getToken().getColor())
            {
                l++;
            }
            else break;
        }
        j1=StartJ;
        i1=StartI;
        while (i1<FieldSize&& j1>0)
        {
             j1--;
            if  (j1%2==0) i1++;
            if (i1==FieldSize) break;
            if (cells[i1][j1].getToken().getColor()==a.getToken().getColor())
            {
                l++;
            }
            else break;
        }
        return l;
    }

    private void DeleteTokensLeftDownAndRightUp(Cell[][] cells, Cell a,int StartI, int StartJ)
    {
        int j1=StartJ;
        int i1=StartI;
        while (j1<FieldSize-1 && i1>=0)
        {
            j1++;
            if  (j1%2==1) i1--;
            if (i1<0) break;
            if (cells[i1][j1].getToken().getColor()==a.getToken().getColor())
            {
                CheckCell[i1*FieldSize+j1]=1;
            }
            else break;
        }
        j1=StartJ;
        i1=StartI;
        while (i1<FieldSize&& j1>0)
        {
            j1--;
            if  (j1%2==0) i1++;
            if (i1==FieldSize) break;
            if (cells[i1][j1].getToken().getColor()==a.getToken().getColor())
            {
                CheckCell[i1*FieldSize+j1]=1;
            }
            else break;
        }

        CheckCell[StartI*FieldSize+StartJ]=1;
    }

    //// check leftUp and rightDown
    private int CheckOneTokenLeftUpAndRightDown(Cell[][] cells, Cell a,int StartI, int StartJ)
    {
        int l=1;
        int j1=StartJ;
        int i1=StartI;
        while (j1<FieldSize-1 && i1<FieldSize)
        {
            j1++;
            if  (j1%2==0) i1++;
            if (i1>=FieldSize) break;
            if (cells[i1][j1].getToken().getColor()==a.getToken().getColor())
            {
                l++;
            }
            else break;
        }
        j1=StartJ;
        i1=StartI;
        while (i1>=0&& j1>0)
        {
            j1--;
            if  (j1%2==1) i1--;
            if (i1<0) break;
            if (cells[i1][j1].getToken().getColor()==a.getToken().getColor())
            {
                l++;
            }
            else break;
        }
        return l;
    }

    private void DeleteTokensLeftUpAndRightDown(Cell[][] cells, Cell a,int StartI, int StartJ)
    {
        int l=1;
        int j1=StartJ;
        int i1=StartI;
        while (j1<FieldSize-1 && i1<FieldSize)
        {
            j1++;
            if  (j1%2==0) i1++;
            if (i1>=FieldSize) break;
            if (cells[i1][j1].getToken().getColor()==a.getToken().getColor())
            {
                CheckCell[i1*FieldSize+j1]=1;
            }
            else break;
        }
        j1=StartJ;
        i1=StartI;
        while (i1>=0&& j1>0)
        {
            j1--;
            if  (j1%2==1) i1--;
            if (i1<0) break;
            if (cells[i1][j1].getToken().getColor()==a.getToken().getColor())
            {
                CheckCell[i1*FieldSize+j1]=1;
            }
            else break;
        }

        CheckCell[StartI*FieldSize+StartJ]=1;
    }

}
