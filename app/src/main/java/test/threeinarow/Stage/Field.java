package test.threeinarow.Stage;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import test.threeinarow.Actor.Token;
import test.threeinarow.Main.Assest;

/**
 * Created by Valera on 22.02.2015.
 */
public class Field {

    protected int FieldSize; /// размер поля
    protected int MaxColor; /// количество цветов

    protected float multiHeight; ///коэффициент высоты
    protected float multiWidth; ///коэффициент ширины
    protected float Xstart;  ///координата X начала поля
    protected float Ystart; ///координата Y начала поля

    protected float indent = 10; /// отступ
    protected byte StrokeWidthLine; ///размер шрифта таблицы

    protected int Score; ///общий счет
    protected int[] ScoreArray; // массив размером FieldSize (количество убранных линий длиной i)

    protected Cell[][] cell=null; //массив ячеек
    protected byte[] CheckCell; //массив обозначений для удаления ячеек

    public Field() {
    }

    //// public///////

    public int[] getScoreArray()
    {
        return ScoreArray;
    }

    public void setScoreArray(int i, int a)
    {
        if (FieldSize>i)
        {
            ScoreArray[i]=a;
        }
    }
    public int getFieldSize() {
        return FieldSize;
    }

    public int getScore() {
        return Score;
    }

    public int getMaxColor() {
        return MaxColor;
    }

    public void setScore(int a) {
        Score = a;
    }

    public void Destroy()
    {
        ScoreArray=null;
        CheckCell=null;

        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                cell[i][j].Destroy();
                cell[i][j]=null;
            }
        }
    }
    ////метод рисования!
    public void Draw(Canvas canvas, Paint paint) {

    }

    ///проверка что фишки находяться рядом(переопределять)
    public boolean BesideToken(Token a, Token b) {
        return false;
    }

    //// проверка нажатия
    public Cell hit(float x, float y)
    {
        for (int i =0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                if (cell[i][j]!=null) {
                    if (cell[i][j].hit(x, y) != null) return cell[i][j].hit(x, y);
                }
            }
        }

        return null;
    }

    public Cell[][] getCell()
    {
        return cell;
    }

    ////проверка всех фишек на анимацию!
    public boolean CheckALlAnimation()
    {
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                if (cell[i][j]!=null)
                {
                    if (cell[i][j].getToken().getRunUpdate()) return true;
                }
            }
        }
        return false;
    }

    ///логика(переопределять!)
    public void Logic(){
    }

    public boolean CheckStep(Cell a , Cell b)
    {
        return false;
    }

    public boolean checkAllStep()
    {
        return false;
    }

    ////обновление поля
    public void newField()
    {
        CreateField();
    }

    public void SwapCell(Token a, Token b)
    {
        cell=SwapCell(cell,a,b);
    }

    //перемешение двух ячеек с помощью фишек
    protected Cell[][] SwapCell(Cell[][] cells,Token a, Token b)
    {
        int i1=0,i2=0,j1=0,j2=0;
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                if (cells[i][j].getToken()==a)
                {
                    i1=i;
                    j1=j;
                }

                if (cells[i][j].getToken()==b)
                {
                    i2=i;
                    j2=j;
                }
            }
        }

        cells=SwapCell(cells,i1,j1,i2,j2);

        return cells;
    }

    ////перемешение двух ячеек через их координаты
    protected Cell[][] SwapCell(Cell[][] cells,int ai, int aj, int bi, int bj)
    {
        Cell c = cells[ai][aj];
        cells[ai][aj] = cells[bi][bj];
        cells[bi][bj] = c;
        return cells;
    }

    ///добавление новых ячеек
    protected void AddNewCell()
    {
        int NumberNull;
        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                if (cell[i][j]==null)
                {
                    NumberNull=0;
                    for (int k=FieldSize-1; k>=0; k--)
                    {
                        if (cell[k][j]==null) NumberNull++;
                        else {
                            CellAnimation(cell[k][j],NumberNull);
                            int i1=k;
                            while (i1<(FieldSize-1))
                            {
                                if (cell[i1+1][j]==null) cell=SwapCell(cell,i1,j,i1+1,j);
                                else break;
                                i1++;
                            }
                        }
                    }
                    for (int k=NumberNull-1; k>=0; k--) {
                        cell[k][j] = CreateCell(k-NumberNull, j);
                        CellAnimation(cell[k][j],NumberNull);
                    }
                }
            }
        }
    }

    /// активация анимации на n ячеек
    protected void CellAnimation(Cell a,int n)
    {
        a.getToken().StartAnimation(a.getToken().getX(),a.getToken().getY()+multiHeight*n);
    }

    ///создание поля
    protected void  CreateField()
    {
        this.StrokeWidthLine=(byte)Math.abs(FieldSize>10? 2 : FieldSize);

        for (int i =0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                cell[i][j]=CreateCell(i,j);
            }
        }
    }

    /// создание ячейки (метод переопределяется для каждого поля)
    protected Cell CreateCell (int i , int j)
    {
        return  null;
    }

    ///копия Cell[][] с заменеными полями
    protected Cell[][] newCells(Cell a, Cell b)
    {
        Cell[][] cells=new Cell[FieldSize][FieldSize];
        int i1=0,i2=0,j1=0,j2=0;


        for (int i=0; i<FieldSize; i++)
        {
            for (int j=0; j<FieldSize; j++)
            {
                if (cell[i][j]==a)
                {
                    i1=i;
                    j1=j;
                }
                else
                if (cell[i][j]==b)
                {
                    i2=i;
                    j2=j;
                }
                else
                    cells[i][j]=new Cell(cell[i][j].getToken().getX(),cell[i][j].getToken().getY(),cell[i][j].getToken().getRadius(),cell[i][j].getToken().getColor());

            }
        }
        cells[i1][j1]=new Cell(cell[i2][j2].getToken().getX(),cell[i2][j2].getToken().getY(),cell[i2][j2].getToken().getRadius(),cell[i2][j2].getToken().getColor());
        cells[i2][j2]=new Cell(cell[i1][j1].getToken().getX(),cell[i1][j1].getToken().getY(),cell[i1][j1].getToken().getRadius(),cell[i1][j1].getToken().getColor());

        return cells;
    }
}
