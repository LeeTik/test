package test.threeinarow.Stage;

import android.graphics.Canvas;
import android.graphics.Paint;

import test.threeinarow.Actor.Token;

/**
 * Created by Valera on 21.02.2015.
 */
public class Cell {
    private Token token;

    public Cell(float x, float y, float radius, int color) {
        token = new Token(x, y, radius, color);
    }

    public void Draw(Canvas canvas, Paint paint)
    {
        token.Draw(canvas, paint);
    }

    public Cell hit(float x, float y)
    {
        return (token.hit(x, y)!=null)? this : null;
    }

    public Token getToken()
    {
        return token;
    }

    public void  setToken(Token a)
    {
        this.token=a;
    }

    public void Destroy()
    {
        token=null;
    }
}
